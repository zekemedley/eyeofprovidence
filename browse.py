#!/usr/bin/env python3
from argparse import ArgumentParser
from logging import basicConfig, debug, info, warning, CRITICAL
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options


FORMAT = '%(asctime)-15s %(levelname)s %(message)s'

def setup_selenium():
    opts = Options()
    opts.headless = True
    browser = Firefox(options=opts)
    return browser

def get_lines(filename):
    with open(filename, "r") as f:
        for line in f:
            if not line.strip():
                break
            yield line.strip()

if __name__ == "__main__":
    parser = ArgumentParser(description="Process some packets")
    parser.add_argument("--protocol", "-p", default="http",
            help="the protocol to use (default: http)")
    parser.add_argument("urlfile", type=str, nargs="+",
            help="files containing one URL per line")
    parser.add_argument('--verbose', '-v', action='count', default=0)

    args = parser.parse_args()
    basicConfig(level=(CRITICAL-args.verbose*10), format=FORMAT)
    browser = setup_selenium()
    for filename in args.urlfile:
        for line in get_lines(filename):
            url = "{}://{}".format(args.protocol, line)
            info("Getting {}".format(url))
            try:
                browser.get(url)
            except Exception as e:
                warning(str(e).strip())
            #print(browser.page_source)
#browser.get('https://duckduckgo.com')
#search_form = browser.find_element_by_id('search_form_input_homepage')
#search_form.send_keys('real python')
#search_form.submit()
#results = browser.find_elements_by_class_name('result')
#print(results[0].text)

