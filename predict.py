#!/usr/bin/env python3
from argparse import ArgumentParser, ArgumentError
from logging import basicConfig, debug, info, CRITICAL
from pcap_analyzer import add_prediction_args, convert_and_scale, get_bidirectional_sessions, load_model, set_args, read_heatmap_data, training_test_split, FORMAT, OUTPUT_LABELS

if __name__ == "__main__":
    parser = set_args()
    parser = add_prediction_args(parser)
    args = parser.parse_args()
    # Start with CRITICAL and knock 10 off for each -v
    basicConfig(level=(CRITICAL-args.verbose*10), format=FORMAT)

    net, output_layer, optimizer, X, Y = load_model(args.load)
    X_test = []
    for pcap_file in args.pcap:
        debug("Reading in: {}".format(pcap_file))
        sessions = get_bidirectional_sessions(pcap_file)
        for session in sessions:
            x, y, z = read_heatmap_data(sessions[session], None, None, None, args)
            X_test = (list(z.reshape(-1)),)
            X_test = convert_and_scale(X_test)

            # Make predictions
            pred = net.run(output_layer, feed_dict={X: X_test})
            total = len(pred)
            for i in range(0, total):  # for each predition...
                predictions = []
                for j in range(0, len(pred[i])):
                    # Check each output for this prediction
                    matched = pred[i][j] > 1-args.test_confidence
                    if matched:
                        predictions.append(OUTPUT_LABELS[j])
                if len(predictions) > 0:
                    print("{} = {}".format(session, ",".join(predictions)))
                else:
                    print("{} = UNKNOWN".format(session))
